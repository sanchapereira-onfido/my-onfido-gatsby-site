import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout"

export default ({ data }) => (
  <Layout>
    <h1>My blog</h1>
    <h4>{data.allMarkdownRemark.totalCount} posts</h4>
    {data.allMarkdownRemark.edges.map(({ node }) => (
      <div key={node.id}>
        <Link
          to={node.fields.slug}
          style={{ textDecoration: "none", color: "inherit" }}
        >
          <h3>{node.frontmatter.title}</h3>
          <span style={{ marginLeft: "20px" }}>{node.frontmatter.date}</span>
          <p>{node.excerpt}</p>
        </Link>
      </div>
    ))}
  </Layout>
)

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM YYYY")
          }
          excerpt
          fields {
            slug
          }
        }
      }
    }
  }
`
