import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout"

export default ({ data }) => (
  <Layout>
    <h1>My files</h1>
    <table>
      <thead>
        <tr>
          <th>path</th>
          <th>extension</th>
          <th>creation time</th>
        </tr>
      </thead>
      <tbody>
        {data.allFile.edges.map(({ node }, index) => (
          <tr key={index}>
            <td>{node.relativePath}</td>
            <td>{node.extension}</td>
            <td>{node.birthTime}</td>
          </tr>
        ))}
      </tbody>
    </table>
  </Layout>
)

export const query = graphql`
  query {
    allFile {
      edges {
        node {
          relativePath
          extension
          birthTime(fromNow: true)
        }
      }
    }
  }
`
